#!/bin/sh
/bin/cp -R /SITE_wp/* /var/www/html/
/bin/chmod 640 /var/www/html/wp-config.php
/bin/chown -R www-data:www-data /var/www/html/wp-content
/bin/chown -R root:www-data /var/www/html
#/bin/chmod 770 /var/www/html/.htaccess
/usr/bin/find /var/www/html -type d -exec chmod 755 {} \;
/usr/bin/find /var/www/html -type f -exec chmod 644 {} +
/usr/bin/find /var/www/html -type f -exec chown root.www-data {} \;
/bin/sh
