<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

define('DISALLOW_FILE_EDIT',true); //edits
define('DISALLOW_FILE_MODS',false); //updates 

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'SITE_DB_NAME');

/** MySQL database username */
define('DB_USER', 'SITE_DB_USER');

/** MySQL database password */
define('DB_PASSWORD', 'SITE_DB_PASSWORD');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8^)<8AaQq7;x*)A!^64.dpGWGBiHg8k3cq_qk2T.MKcdh:fChnZA8-~*#my8c$TA');
define('SECURE_AUTH_KEY',  '-S W:;2@hE;].i|ELVhvYyQb?@{Cm,H%&9oU{2~@p~n5j%Wd}{;i&)$bDZ#sF0?#');
define('LOGGED_IN_KEY',    'n{QY};J*xi0]k>E)]bY1^8;#~<gJM6]u0Fw @ $YP.cn~Eu/n69[&p ;m7*Y?fl6');
define('NONCE_KEY',        'qCrI>>1DAoWX7 rTgE9qXHb2s$R~@CK}{eNG$ 40?<?Kntl&B28:rw6Sk+T`1Fw$');
define('AUTH_SALT',        'r~d1MQ)T;hNRS_gw{+&%qRkFB{ zLtAC&ym.eKy!fl=Qp9R YqaR ;u/fFws[qqP');
define('SECURE_AUTH_SALT', ':phu.O2pogz^e7[h/yZJ^FzDvuZWl%zzLjc{YtgHrfUsef.OV9+]lXqtLHqiTr^T');
define('LOGGED_IN_SALT',   '}q)?0!KBzeE^+dO.i}jUl<2|L:tDqc-<f2s,lo(e<V?)pzq#O_{b{GqR8{H)8U^p');
define('NONCE_SALT',       'hv+%Z_ZMHTu-CJ6u_vFU8MamKeQ!mYQ5dxnU-9. G1U&LI_K?v/iV`zc+>.3$mna');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


